/*
 * dump.c
 *
 *  Created on: Nov 30, 2019
 *      Author: user
 */
#include "main.h"
#include <stdio.h>
#include "xprintf.h"

static char toHex(uint8_t val)
{
    if (val >= 0 && val <= 9)
        return (char)val + '0';
    if (val >= 0xA && val <= 0xF)
        return (char)val - 10 + 'A';
    return 0;
}

void dump16(uint16_t *data, int len)
{
    char buf[12 * 5 + 3];
    char* curr = buf;

//    int size = 0;

    for (int i = 0; i < len; i++)
    {
        uint16_t temp = data[i];
        for (int j = 0; j<4; j++)
        {
            uint8_t v = temp & 0xF;
            temp >>= 4;
            *curr++ = toHex(v);
        }
//        curr += sprintf(curr, "%X", data[i]);
        if (i != len - 1)
        {
            *curr++ = ' ';
            *curr = 0;
        }
    }
    xprintf("%s\r\n", buf);
    //HAL_UART_Transmit(huart, (uint8_t *)buf, size, 100);
}

void dump8(uint8_t *data, int len)
{
    char buf[12 * 5 + 3];
    char* curr = buf;

//    int size = 0;

    for (int i = 0; i < len; i++)
    {
        uint16_t temp = data[i];
        for (int j = 0; j < 2; j++)
        {
            uint8_t v = temp & 0xF;
            temp >>= 4;
            *curr++ = toHex(v);
        }
        if (i != len - 1)
        {
            *curr++ = ' ';
            *curr = 0;
        }
    }
    xprintf("%s\r\n", buf);
}
