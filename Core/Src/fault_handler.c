/*
 * fault_handler.c
 *
 *  Created on: Oct 9, 2019
 *      Author: dsemenets
 */
#include <stdio.h>
#include "main.h"
#include "core_cm3.h"

void printErrorMsg(const char * errMsg);
void printUsageErrorMsg(uint32_t CFSRValue);
void printBusFaultErrorMsg(uint32_t CFSRValue);
void printMemoryManagementErrorMsg(uint32_t CFSRValue);
void stackDump(uint32_t stack[]);


void Hard_Fault_Handler(uint32_t stack[])
{
   //if((CoreDebug->DHCSR & 0x01) != 0) {
	xputs("\n---------------------------------\nHard Fault");
	xprintf("SCB->HFSR = 0x%08x\n", SCB->HFSR);
	if ((SCB->HFSR & (1 << 30)) != 0)
	{
		xprintf("Forced Hard Fault\n");
		xprintf("SCB->CFSR = 0x%08x\n", SCB->CFSR );
		if((SCB->CFSR & 0xFFFF0000) != 0)
			printUsageErrorMsg(SCB->CFSR);
		if((SCB->CFSR & 0xFF00) != 0)
			printBusFaultErrorMsg(SCB->CFSR);
		if((SCB->CFSR & 0xFF) != 0)
			printMemoryManagementErrorMsg(SCB->CFSR);
	}
	stackDump(stack);
	asm("BKPT #01");
	//}
}

void printUsageErrorMsg(uint32_t CFSRValue)
{
	xprintf("Usage fault: ");
	CFSRValue >>= 16; // right shift to lsb

	if (CFSRValue & (1<<0))
		xprintf("Undefined Instruction\n");
	if (CFSRValue & (1<<1))
		xprintf("Invalid state\n");
	if (CFSRValue & (1<<2))
		xprintf("Invalid PC\n");
	if (CFSRValue & (1<<3))
		xprintf("No Co-Processor\n");
	if (CFSRValue & (1<<8))
		xprintf("Unaligned access\n");
	if (CFSRValue & (1<<9))
		xprintf("Divide by zero\n");
}

void printBusFaultErrorMsg(uint32_t CFSRValue)
{
	xprintf("Bus fault: ");
	CFSRValue = ((CFSRValue & 0x0000FF00) >> 8); // mask and right shift to lsb
	if (CFSRValue & 0x1)
		xprintf("Instruction bus error\n");
	if (CFSRValue & 0x2)
		xprintf("Precise data bus error\n");
	if (CFSRValue & 0x4)
		xprintf("Imprecise data bus error\n");
	if (CFSRValue & 0x8)
		xprintf("BusFault on unstacking for a return from exception\n");
	if (CFSRValue & 0x10)
		xprintf("BusFault on stacking for exception entry\n");
	if (CFSRValue & 0x80)
		xprintf("Fault address 0x%08x\n", SCB->BFAR);
}

void printMemoryManagementErrorMsg(uint32_t CFSRValue)
{
	xprintf("Memory Management fault: ");
	CFSRValue &= 0x000000FF; // mask just mem faults
	if (CFSRValue & (1<<0))
		xprintf("Instruction access violation");
	if (CFSRValue & (1<<1))
		xprintf("Data access violation");
	if (CFSRValue & (1<<3))
		xprintf("fault on unstacking for a return from exception");
	if (CFSRValue & (1<<4))
		xprintf("fault on stacking for exception entry");
	if (CFSRValue & (1<<7))
		xprintf(" at address 0x%08x", SCB->MMFAR);
	xputs("");
}

#if 0
#if defined(__CC_ARM)
__asm void HardFault_Handler(void)
{
   TST lr, #4
   ITE EQ
   MRSEQ r0, MSP
   MRSNE r0, PSP
   B __cpp(Hard_Fault_Handler)
}
#elif defined(__ICCARM__)
void HardFault_Handler(void)
{
   __asm(   "TST lr, #4          \n"
            "ITE EQ              \n"
            "MRSEQ r0, MSP       \n"
            "MRSNE r0, PSP       \n"
            "B Hard_Fault_Handler\n"
   );
}
#elif defined(__GNUC__)
void HardFault_Handler(void)
{
	asm(
		"TST lr, #4          \n"
		"ITE EQ              \n"
		"MRSEQ r0, MSP       \n"
		"MRSNE r0, PSP       \n"
		"B Hard_Fault_Handler\n"
	);
}
#else
	#warning Not supported compiler type
#endif
#endif
enum { r0, r1, r2, r3, r12, lr, pc, psr};

void stackDump(uint32_t stack[])
{
	xprintf("r0  = 0x%08x\n", stack[r0]);
	xprintf("r1  = 0x%08x\n", stack[r1]);
	xprintf("r2  = 0x%08x\n", stack[r2]);
	xprintf("r3  = 0x%08x\n", stack[r3]);
	xprintf("r12 = 0x%08x\n", stack[r12]);
	xprintf("lr  = 0x%08x\n", stack[lr]);
	xprintf("pc  = 0x%08x\n", stack[pc]);
	xprintf("psr = 0x%08x\n", stack[psr]);
	xputs("-------------------");
	xprintf ("BFAR = 0x%08x\n", SCB->BFAR);
	xprintf ("CFSR = 0x%08x\n", SCB->CFSR);
	xprintf ("HFSR = 0x%08x\n", SCB->HFSR);
	xprintf ("DFSR = 0x%08x\n", SCB->DFSR);
	xprintf ("AFSR = 0x%08x\n", SCB->AFSR);
	xputs("-------------------");
}
