/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2020 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdbool.h>
#include "xprintf.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
extern uint32_t timer;
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void dump16(uint16_t *data, int len);
void dump8(uint8_t *data, int len);
void ProcessSpectr(uint16_t *spectr);
uint16_t *GetSpectr(uint16_t *dst);
/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define BUS_FREQ 72000
#define T_PERIOD BUS_FREQ/800
#define T_RESET 0
#define T0H 26
#define T1H 65
#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define WS2812_OUT_Pin GPIO_PIN_0
#define WS2812_OUT_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */
#define ANIM_CHANGE_COUNTER 600  // 60 sec
#define SOUND_TIMEOUT 50  // 5 sec
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
