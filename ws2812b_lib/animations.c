#include <stdlib.h>
#include <stdbool.h>
#include <memory.h>
#include "animations.h"
#include "colors.h"
#include "main.h"

static inline void ClearFrame(led_unit_p frame);
static inline void FillFrame(led_unit_p frame, led_unit_t val) ;
/**
 *
 */
Animation_function volatile Animation;
static bool start_frame = true;

struct Animation Animations[] =
{
        {Animation_RunningLed, "Running led"},
        {Animation_RunningLed2, "Running led 2"},
        {Animation_Pulse, "Pulse"},
        {Animation_RunningLine, "Running Line"},
        {Animation_Train, "Train"},
        {Animation_DoubleTrain, "Double Train"},
        {Animation_Rainbow, "Rainbow"},
        {Animation_Snake, "Snake"},
        {Animation_Stars, "Stars"},
        {Animation_RoundDance, "Round Dance"},
        {Animation_MarbleTube, "Marble Tube"},
        {(Animation_function)NULL, ""},
};

/**
 *
 */
static inline void ClearFrame(led_unit_p frame)
{
	for(uint32_t i = 0; i < STRIP_LEDS_NUM; i++)
	{
		SET_COLOR(&frame[i], LED_OFF);
	}
}

/**
 *
 */
static inline void FillFrame(led_unit_p frame, led_unit_t val)
{
	for(uint32_t i = 0; i < STRIP_LEDS_NUM; i++)
	{
		SET_COLOR(&frame[i], *(uint32_t*)&val);
	}
}


/**
 *
 */
void SetAnimation(Animation_function Animation_function)
{
	Animation = Animation_function;
	start_frame = true;
}

void Animation_Custom(HSV_p leds_frame, __attribute__ ((unused)) HSV_p notUsed)
{

}
/**
 *
 */
void Animation_RunningLed(HSV_p leds_frame, __attribute__ ((unused)) HSV_p notUsed)
{
	static uint32_t frame = 0, stage = 0, color = HSV_GREEN;
	const uint32_t preset_colors[8] = {HSV_GREEN, HSV_RED, HSV_BLUE, HSV_YELLOW, HSV_MAGENTA, HSV_CYAN, HSV_WHITE, LED_OFF};
	
	ClearFrame(leds_frame);

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
        stage = 0;
    }
	if (stage % 2 == 0) // прямой ход
	{
		SET_COLOR(&leds_frame[frame], color);
	}
	else // обратный ход
	{
		SET_COLOR(&leds_frame[STRIP_LEDS_NUM-1-frame], color);
	}
	
	frame++;
	if (STRIP_LEDS_NUM == frame)
	{
		frame = 0;
		stage++;
		if (stage % 2 == 0)
		{
			if (stage < 13)
			{
				color = preset_colors[stage/2];
			}
			else if (stage == 14)
			{
				stage = 0;
				color = preset_colors[0];
			}
			else
			{
				color = LED_OFF;
			}
		}
	}
}


/**
 *
 */
void Animation_RunningLed2(HSV_p leds_frame, HSV_p prev_frame)
{
	static uint32_t frame = 0;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
	for (uint16_t i = 0; i < STRIP_LEDS_NUM-1; i++)
	{
		leds_frame[i+1] = prev_frame[i];
	}

	if (frame % 16 == 0)
	{
		leds_frame[0] = GetRandomHsvColor();
	}
	else
	{
		leds_frame[0] = HSV_ModBrightness(prev_frame[0], -5);
	}

	frame++;
}

/**
 *
 */
void Animation_Pulse(HSV_p leds_frame, __attribute__ ((unused)) HSV_p notUsed)
{
	const uint32_t preset_colors[8] = {HSV_GREEN, HSV_RED, HSV_BLUE, HSV_YELLOW, HSV_MAGENTA, HSV_CYAN, HSV_WHITE, LED_OFF};
	HSV_t color;
	static uint8_t frame = 0, stage = 0;
	
    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
        stage = 0;
    }
	SET_COLOR(&color, preset_colors[stage]);

	if (frame > 0x40)
	{
		color.V = 0x80 - frame;
	}
	else
	{
		color.V = frame;
	}

	frame++;

	if (frame == 0x80)
	{
		frame = 0;
		stage++;
		if (stage == 7)
		{
			stage = 0;
		}
	}
		
	FillFrame(leds_frame, color);
}

/**
 *
 */
void Animation_RunningLine(HSV_p leds_frame, __attribute__ ((unused)) HSV_p notUsed)
{
	static uint32_t frame = 0, stage = 0, color = HSV_GREEN;
	const uint32_t preset_colors[7] = {HSV_GREEN, HSV_RED, HSV_BLUE, HSV_YELLOW, HSV_MAGENTA, HSV_CYAN, HSV_WHITE};

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
        stage = 0;
    }
	for (uint32_t i = 0; i < frame; i++)
	{
		SET_COLOR(&leds_frame[i], color);
	}
	
	frame++;
	if (frame > STRIP_LEDS_NUM+1)
	{
		frame = 0;
		if (color == LED_OFF) 
		{
			stage++;
			if (stage == 7) 
			{
				stage = 0;
			}
			color = preset_colors[ stage ];
		}
		else
		{
			color = LED_OFF;
		}	
	}
}

/**
 *
 */
void Animation_Train(HSV_p leds_frame, __attribute__ ((unused)) HSV_p notUsed)
{
	static led_unit_t train[5];
	static int32_t train_position = -3;
	static int8_t speed;
	
	ClearFrame(leds_frame);

	if (train_position <= -3) 
	{
		speed = 1;
		HSV_t color = GetRandomHsvColor();
		color.V = 0x10;
		train[0] = color;
		color.V = 0x20;
		train[1] = color;
		color.V = 0x30;
		train[2] = color;
		color.V = 0x40;
		train[3] = color;
		color.V = 0x38;
		train[4] = color;
	}
	
	for (uint8_t i = 0; i < 5; i++)
	{
		int32_t carriage_position = train_position + i*speed;

		if (carriage_position >= 0 && carriage_position < STRIP_LEDS_NUM)
		{
			leds_frame[carriage_position] = train[i];
		}
	}
	
	train_position += speed;
	
	if (train_position > STRIP_LEDS_NUM+3)
	{
		speed = -1;
	}
}

/**
 *
 */
void Animation_DoubleTrain(HSV_p leds_frame, HSV_p prev_frame)
{
	static HSV_t color1, color2;
	static uint32_t position = STRIP_LEDS_NUM,
					frame = 5;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 5;
        position = STRIP_LEDS_NUM;
    }
	for (uint32_t i = 0; i < STRIP_LEDS_NUM; i++)
	{
		leds_frame[i] = HSV_ModBrightness(prev_frame[i], -1);
	}

	if (position == STRIP_LEDS_NUM)
	{
		position = 0;
		color1 = GetRandomHsvColor();
		color2 = GetRandomHsvColor();
	}

	leds_frame[position] = color1;
	leds_frame[STRIP_LEDS_NUM-1 - position] = color2;

	position++;
	frame++;
}

static inline HSV_t getNextRainbowColor(HSV_t current, const uint8_t step)
{
	current.H += step;
	if (current.H >= MAX_H_VALUE)
	{
		current.H -= MAX_H_VALUE;
	}
	return current;
}
/**
 *
 */
void Animation_Rainbow(HSV_p leds_frame, HSV_p prev_frame)
{
	const uint8_t step = MAX_H_VALUE / STRIP_LEDS_NUM;
	static uint32_t frame = 0;
	
    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
	if (!frame)
	{
		leds_frame[0] = hsv(0, 255, 0x40);
	}
	else
	{
		leds_frame[0] = getNextRainbowColor(prev_frame[0], 1);
		
	}
	for(uint16_t i = 1; i < STRIP_LEDS_NUM; i++)
	{
		leds_frame[i] = getNextRainbowColor(leds_frame[i-1], step);
	}
	frame++;
}

/**
 *
 */
#define SNAKE_LENGTH		16

void Animation_Snake(HSV_p leds_frame, __attribute__ ((unused)) HSV_p notUsed)
{
	static uint32_t	frame = 0;
	static HSV_t	snake_colors[SNAKE_LENGTH];
	static uint16_t	snake_positions[SNAKE_LENGTH];
	static HSV_t	food_color;
	static uint16_t food_position;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
	for (uint8_t i = SNAKE_LENGTH-1; i > 0; i--) // последние 2 должны быть чёрными, чтобы затирать хвост.
	{
		snake_positions[i] = snake_positions[i-1];
		leds_frame[snake_positions[i]] = snake_colors[i];
	}

	if (snake_positions[0] > food_position)
	{
		snake_positions[0]--;
	}
	else
	{
		snake_positions[0]++;
	}

	if (snake_positions[0] == food_position)
	{
		for (uint8_t i = SNAKE_LENGTH-3; i > 0; i--) // последние 2 должны быть чёрными, чтобы затирать хвост.
		{
			snake_colors[i] = snake_colors[i-1];
		}
		snake_colors[0] = food_color;
		food_color = GetRandomHsvColor();
		food_position = rand() % STRIP_LEDS_NUM;
	}

	leds_frame[food_position] = food_color;
	leds_frame[snake_positions[0]] = snake_colors[0];

	frame++;
}

/**
 *
 */
#define STARS_NUM	10

void Animation_Stars(HSV_p leds_frame, HSV_p prev_frame)
{
#define		STAR	leds_frame[stars[i]]

	static uint32_t stars[STARS_NUM];
	static uint32_t frame = 0;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
	for (uint16_t i = 0; i < STRIP_LEDS_NUM; i++) {
		leds_frame[i] = HSV_ModBrightness(prev_frame[i], -1);
	}

	for (uint8_t i = 0; i < STARS_NUM; i++)
	{
		if (rand() < (RAND_MAX / 4))
		{
			STAR = HSV_ModBrightness(STAR, 3);
		}

		if (STAR.V < 4)
		{
			STAR.V = 0;
			stars[i] = rand() % STRIP_LEDS_NUM;
			STAR = GetRandomHsvColor();
		}
	}

	frame++;

#undef STAR
}

/**
 *
 */
void Animation_RoundDance(HSV_p leds_frame, HSV_p prev_frame)
{
	static uint32_t frame = 0;
	static uint16_t H = 0;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
	for (uint16_t i = STRIP_LEDS_NUM-1; i > 0; i--)
	{
		leds_frame[i] = prev_frame[i-1];
	}

	if (frame % 16 == 0) // 16 separation
	{
		leds_frame[0] = hsv(H, 0xFF, MAX_V_VALUE);
	}
	else
	{
		leds_frame[0].V = 0;
	}

	H++;
	if (H == MAX_H_VALUE)
	{
		H = 0;
	}
	frame++;
}


/**
 *
 */
void Animation_MarbleTube(HSV_p leds_frame, HSV_p prev_frame)
{
	static uint32_t frame = 0;
	static uint16_t end = STRIP_LEDS_NUM; // последний стоящий СД
	uint16_t i;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
	for (i = end-1; i > 0; i--)
	{
		leds_frame[i] = prev_frame[i-1];
	}

	if (frame % 16 == 0 )
	{
		leds_frame[0] = GetRandomHsvColor();
	}
	else
	{
		SET_COLOR(&leds_frame[0], LED_OFF);
	}

	leds_frame[end] = prev_frame[end];

	// wave pulse
	for (i = STRIP_LEDS_NUM-1; i > end; i--)
	{
		leds_frame[i].S = prev_frame[i-1].S; // передача пульсации
	}

	if (end < STRIP_LEDS_NUM) // сглаживание пульсации
	{
		uint16_t S = leds_frame[end].S + 0x01;
		if (S > 0xFF)
		{
			S = 0xFF;
		}
		leds_frame[end].S = S;
	}

	if (end < STRIP_LEDS_NUM	&&	(frame % 32 == 0)) // для пульсации уже стоящих, чтобы скучно не было.
	{
		leds_frame[end].S = 0x10;
	}


	if (leds_frame[end-1].V != 0) // прицепился ещё один
	{
		end--; // увеличиваем цепь стоящих

		if (end == 0) // длина цепи = вся лента
		{ // restart
			end = STRIP_LEDS_NUM-1;
			ClearFrame(leds_frame);
			frame = 0;
		}
	}

	frame++;
}

#define CHANNELS_NUM 6

#if STRIP_LEDS_NUM / CHANNELS_NUM == 0
#define LEDS_PER_CHAN 1
#else
#define LEDS_PER_CHAN (STRIP_LEDS_NUM / CHANNELS_NUM)
#endif
#if CHANNELS_NUM == 12
const static uint32_t levels[] = {2047, 1947, 1656, 1203, 633, 0};
const static uint16_t koeff[] = {2000, 1906, 1679, 1416, 1183, 1000};
const static uint16_t hues[] = {0, 128, 256, 384, 512, 640, 768, 896, 1024, 1152, 1280, 1408};
#define EFFECT
#elif CHANNELS_NUM == 6
const static uint32_t levels[] = {2047, 2026, 1964, 1862, 1722, 1547, 1340, 1107, 850, 577, 291, 0};
const static uint16_t koeff[] = {2000, 1979, 1922, 1834, 1726, 1607, 1487, 1370, 1262, 1164, 1077, 1000};
const static uint16_t hues[] = {0, 256, 512, 768, 1024, 1280};
#define EFFECT
#endif
static uint16_t spectr[12];
HSV_p  SetValue(int channel, uint16_t value, HSV_p buf)
{
    int i;
#if defined(EFFECT)
    for (i=0; i<LEDS_PER_CHAN/2; i++)
    {
        volatile uint32_t level;
        if (value < levels[i])
        {
            buf[i].V = 0;
            buf[LEDS_PER_CHAN-1-i].V = 0;
        }
        else
        {
			level = (((uint32_t)value-levels[i])*koeff[i]/1000)>>4;
			buf[i].V = level;
			buf[LEDS_PER_CHAN-1-i].V = level;
        }
        buf[i].H = hues[channel];
        buf[LEDS_PER_CHAN-1-i].H = hues[channel];
        buf[i].S = 255;
        buf[LEDS_PER_CHAN-1-i].S = 255;
    }
    return buf+LEDS_PER_CHAN;
#else
    for (i=0; i<LEDS_PER_CHAN; i++)
    {
    	buf->V = (uint8_t)(value >> 4);
    	buf++;
    }
    return buf;
#endif
}

void Animation_SoundLights(HSV_p leds_frame, HSV_p prev_frame)
{
    static uint32_t frame = 0;
    int i;
	HSV_p buff;

    if (unlikely(start_frame))
    {
        start_frame = false;
        frame = 0;
    }
    GetSpectr(spectr);
    for (i = 0, buff=leds_frame; i < CHANNELS_NUM; i++)
    {
    	buff = SetValue(i, spectr[i], buff);
    }
    frame++;
}
